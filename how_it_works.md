# How Coda Works

![High Level Server Architecture](https://zxfcnq.ch.files.1drv.com/y4mg6VCFf7kPWS3i3BbFEkCNsnKY6GwXyHRPgAb1HcJgmuiliARiEOlUO57jl7RwVYi4_sUguuK09aMDuuRoRXT3B6a4olkUt6NBKZUEKoa7lpmYHlR3zU6IQsxRydEN2UWYefthsuyA9eX6XTHLYaqE4ibnYuNxVe5RoydoARPQ_KD59f7mDykHw1c7E43-G5cb1TwFZBMHp7hjU_RRuRVtQ?width=710&height=302&cropmode=none)

As you can see, from a high-level Coda is very simple, consisting of only a few main processes between handling a request and providing a response. We will discuss each process in depth in later, but for now, we will cover the basics of how a Coda server handles requests for data access.

## Terms used
First let's learn a few key terms.

* An interface, for our purposes, is simply a method of interacting with the data. This could be a web application, mobile, application, a service, or really anything that is performing requests on behalf of something else, be that a person, another service, etc...
* A request is just that. The request that an interface is making.
* Resource/Asset Resolution is the method of identifyng what resource or asset on the server is being requested.
    * A resource is simply information. This information could be about anything, and can contain and/or describe assets.
    * An asset is something that is not information, such as an image, video, or audio. This also includes things like stylesheets and scripts. Assets inherit their permissions from their parent resource.
* Authentication is the method of ensuring that someone is who they say they are.
* Authorization is the method of ensuring that the authenticated person has access to a resource or asset.
* Resource/Asset Processing is the method of processing a resource or asset, and preparing it for the response.
* Accounting is the method of documenting what was requested by whom, and what was returned. This means that you can audit what was being requested and make more informed decisions about how visible your data is. It is also very useful when reporting issues.
* Response is the data from the resource, or asset of the resource, that is returned to authorized entity if the resource or asset exists and they are authorized to view it. The response will be an error message if the resource or asset does not exist, or the requesting entity is not authorized to view it.

## Going full circle

1. An entity uses an application. for our example, let's say it's social service, which uses an aggregation service, and they find your CodaID. They click on a link to view your social profile. The application is the **interface**. An aggregation service is a service **interface** that a Coda server connects to, informing the server what identities are there. More on that later.
2. The application, our interface, makes a **request** to a Coda server. This request contains information about what they are requesting and from where and how they are making the request.
3. The resource/asset resolution process takes the information from the request, normalizes it, collects it, if it exists, and sends it to the next step.
4. The first of the more complex processes is authentication. Here, Coda check to see if the entity is authenticated or not, and decides how to continue on several variables. More on that later.
5. In the authorization stage, Coda determines if an entity can do what they requested on the resource/asset. As with authorization, this step takes into account a lot of variables on how to conitnue. It also builds a logic tree for the next step.
6. In the resource/asset processing, the response data is built and any information that is not allowed to be returned is filtered out.
7. The server tells the accounting process all about the request, such as the data requested, by who, where, when, and how, and what the response is.
8. The server sends the data to the response process to build the response.
9. The application receives the response and begins processing it for viewing.