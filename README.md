# Common Open Data Access 1.0

> **Abstract**  
> Common Open Decentralized Access is a collection of resources providing guidance and utilities for implementing a decentralized Internet. It enables owners to maintain their right to their data while providing developers a consistent method to interact with it using a REST-like service.
>   
> This specification defines the Common Open Decentralized Access components, their functionality, and communication between them. It also defines the security and privacy considerations and requirements for using Common Open Decentralized Access.

## Contents

1. [Introduction](#introduction)
    1. [Requirements Notation and Conventions](#requirements-notation-and-conventions)
    2. [Terminology](#terminology)
    3. [Overview](#overview)



## Introduction
Common Open Decentralized Access, or simply **Coda**,  is a collection of resources providing guidance and utilities for implementing a decentralized Internet. It enables owners to maintain their right to their data while providing developers a consistent method to interact with it using a REST-like service.

The *Common Open Data Access 1.0* specification defines the core components of Coda, their functionality, and communication between components. It also defines the security and privacy considerations and requirements for using Common Open Decentralized Access.

As background, the current state of the web invites concern when it comes to data access. The concern is privacy as companies provide free services in exchange for your data privacy. Companies have been treating their user's personal data as a commodity, selling to others, in order to create a profitable company.

IntegerEleven Technologies, the authors of Coda, believe that data, at its core, represents moments in life; be that a personal experience, a sell or purchase, an idea, a collaboration, etc... They believe that the owners of data are those that created it, or those for whom it was created. 

Coda utilizes current, stable and well documented specifications as a means to provide all aspects of a centralized application over a decentralized Internet.

### Requirements Notation and Conventions
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119].

In the .txt version of this document, values are quoted to indicate that they are to be taken literally. When using these values in protocol messages, the quotes MUST NOT be used as part of the value. In the HTML version of this document, values to be taken literally are indicated by the use of `this fixed-width font`.

<!--TODO Link to other specifications used -->

### Teminology
This specification uses several terms.
<!--TODO Include Terms and Link to other spec as needed-->


### Overview

The Coda communication specification, in abstract, follows the following steps.

1. A user using CI (client interface) performs a request of owner data maintained on CS (data owner's Coda Server).
2. OC sends the request to RQ (request processing) to find the requested data and get its ACL.
3. RQ sends the request and ACL to the AN (authentication) to determine who is making the request for the dataa.
    * If visiting user is on RC (a different remote Coda Server), then authenticate against that server.
4. AN sends request, authenticated user information, and permissions to RR for retrieval of the data.
5. RR sends request, authenticated user information and data to AZ (authorization) to determine permissions to properties of the data.
6. AZ sends request, authenticated user information, data, and property permissions map to RP (resource processing) where the data is accessed and/or modified (if permissions allow). Resultant data is prepared and only authorized properties of the data are extracted. An event tag is created
7. RP sends request, authenticated user information, resultant data, and event tag to AU (auditing) where information about the request, the event, and response are stored.
8. AU sends request and resultant data to RS (response processing) where the appropriate response is generated.
9. RS provides CS with the response.
10. CS sends the response to CI. CI handles response as necessary.


These steps are illustrated below.

```
┌──────┐    ┌──────┐    ┌──────┐    ┌──────┐(3*)┌──────┐    ┌──────┐
|      |(1) |      |(2) |      |(3) |      |<··>|  RC  |    |      |
|      |--->|      |--->|  RP  |--->|  AN  |    └──────┘    |  RR  |─┐
|      |    |      |    |      |    |      |--------------->|      | ¦
|      |    |      |    └──────┘    └──────┘      (4)       └──────┘ ¦
|  CI  |    |  CS  |                                                 ¦ (5)
|      |    |      |    ┌──────┐    ┌──────┐    ┌──────┐    ┌──────┐ ¦
|      |    |      |    |      |    |      |    |      |    |      | ¦
|      |<---|      |<---|  RS  |<---|  AU  |<---|  RP  |<---|  AZ  |<┘
|      |(10)|      | (9)|      | (8)|      | (7)|      | (6)|      |
└──────┘    └──────┘    └──────┘    └──────┘    └──────┘    └──────┘
```

<!--- Link to documents -->
Of course these steps consist of several other processes. These are covered in deptch in other pages.

## Identity
Identity in Coda is implemented using OpenID.
<!-- Touch more on this -->

[RFC 2119]: http://tools.ietf.org/html/rfc2119 "Bradner, S., 'Key words for use in RFCs to Indicate Requirement Levels', BCP 14, RFC 2119, March 1997"